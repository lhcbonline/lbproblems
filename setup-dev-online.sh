#!/bin/bash
#
# Script to configure the environment for running the application int eh LHCB
# Online network
#


export PATH="/home/jcaicedo/local/python/bin:$PATH"

export ORACLE_HOME=/sw/oracle/10.2.0.4/linux64
export LD_LIBRARY_PATH=/sw/oracle/share/lib:/sw/oracle/10.2.0.4/linux64/lib


export PYTHONPATH=/home/jcaicedo/local/Django-1.1.1:$ORACLE_HOME/python:/home/jcaicedo/local/django-apps:/home/jcaicedo/workspace/problemdb/tmp/lib
export PATH=$PATH:/sw/oracle/10.2.0.4/linux64/bin:/sw/oracle/share/bin

