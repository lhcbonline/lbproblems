export ORACLE_HOME=/sw/oracle/10.2.0.4/linux64

export LD_LIBRARY_PATH=/sw/oracle/share/lib:$ORACLE_HOME/lib
export PATH=$PATH:$ORACLE_HOME/bin:/sw/oracle/share/bin

export PythonPath=/web/sites/problemdb/lib:/web/sites/problemdb/releases/June-2011:/web/sites/problemdb/releases/June-2011/lhcb_problemdb:/web/sites/problemdb/releases/June-2011/lib/python2.4/site-packages
