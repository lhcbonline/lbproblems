# -*- coding: utf-8 -*-
#
# Configuration settings for deploying the application in the LHCb network
#

#DEBUG = True
#TEMPLATE_DEBUG = DEBUG


ERROR_LOG_FILE = '/var/log/httpd/problemdb-exception_log'

SECRET_KEY = 'A+ry8)02o!46fav@fm9l4ez3fF-n6m%_lxeHz3H9J5-*44ml!*'

# Settings for using the Oracle develoment database

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.oracle',
#        'NAME': 'INT12R_LB',
        'NAME': 'LHCBR_LB',
        'USER':'lhcb_problemdb',
        'PASSWORD':'1bpr0b1ems',
        'HOST':'',
        'PORT':''
    }
}


# Authentication using the Active Directory
AD_DNS_NAME = 'ldap01.lbdaq.cern.ch'
AD_NT4_DOMAIN = 'LHCB' # This is the NT4/Samba domain name
AD_SEARCH_DN = 'OU=Users,OU=LHCB,dc=lbdaq,dc=cern,dc=ch'
AD_LDAP_PORT = 389

# Authentication using CERN SOAP web services
CERN_WSAUTH_URI = 'https://winservices-soap.web.cern.ch/winservices-soap/Generic/Authentication.asmx?WSDL'
CERN_WSAUTH_USER = 'webauth'
#CERN_WSAUTH_PASSWORD = 'LHCb!soapZ0'
CERN_WSAUTH_PASSWORD = 'MebaGeva7'
CERN_WSAUTH_LISTS_ALLOWED = ('lhcb-general',)

CERN_WSAUTH_CLI_CMD = ["/opt/python/python-2.5.5/bin/python", "/opt/lampp/vhosts/problemdb/releases/wscli/lhcb_problemdb/cernauth/scripts/get-user-info.py"]
CERN_WSAUTH_CLI_PATH = '/opt/lampp/vhosts/problemdb/releases/current/lhcb_problemdb'
CERN_WSAUTH_CLI_SETTINGS = 'settings'

#
META_ADFS_USERNAME = 'HTTP_ADFS_LOGIN'
META_ADFS_FIRSTNAME = 'HTTP_ADFS_FIRSTNAME'
META_ADFS_LASTNAME = 'HTTP_ADFS_LASTNAME'
META_ADFS_EMAIL = 'HTTP_ADFS_EMAIL'
META_ADFS_BUILDING = 'HTTP_ADFS_BUILDING'
META_ADFS_DEPARTMENT = 'HTTP_ADFS_DEPARTMENT'
META_ADFS_FULLNAME = 'HTTP_ADFS_FULLNAME'
META_ADFS_GROUP = 'HTTP_ADFS_GROUP'
META_ADFS_HOMEINSTITUTE = 'HTTP_ADFS_HOMEINSTITUTE'
META_ADFS_PERSONID = 'HTTP_ADFS_PERSONID'
META_ADFS_PHONENUMBER = 'HTTP_ADFS_PHONENUMBER'
