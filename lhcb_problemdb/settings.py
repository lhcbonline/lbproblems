# -*- coding: utf-8 -*-
# Django settings for problemdb project.
#
# Here we define common
#


import os

# Debug is enabled by default. It can be disabled by setting the environment
# variable PROBLEMDB_PRODUCTION. This allows you to quickly test and change the code
# keeping debug information available, and to supress it in the production
# environment.

# The environment variable can be set using the 'SetEnv' directive in the
# VirtualHost configuration file. For example:
# 
# SetEnv PROBLEMDB_PRODUCTION 1
#
DEBUG = not os.getenv('PROBLEMDB_PRODUCTION')
TEMPLATE_DEBUG = DEBUG

# Path to the project files. Used for creating relative paths to other 
# settings, e.g. the path to the sqlite database.
PROJECT_PATH = os.path.abspath(os.path.dirname(__file__))


# Date and time formats used in the templates
DATE_FORMAT = 'Y-m-d'
TIME_FORMAT = 'H:i'
DATETIME_FORMAT = 'Y-m-d H:i'

DATE_FORMAT = DATETIME_FORMAT

FORM_DATE_FORMAT = '%Y-%m-%d'
FORM_TIME_FORMAT = '%H:%M'


ADMINS = (
    # ('Your Name', 'your_email@domain.com'),
)
# Used for the debug toolbar
INTERNAL_IPS = ('127.0.0.1',)

MANAGERS = ADMINS

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(PROJECT_PATH, '..', 'tmp', 'data.sqlite'),
        'USER':'',
        'PASSWORD':'',
        'HOST':'',
        'PORT':''
    }
}



# Local time zone for this installation. Choices can be found here:
# http://en.wikipedia.org/wiki/List_of_tz_zones_by_name
# although not all choices may be available on all operating systems.
# If running in a Windows environment this must be set to the same as your
# system time zone.
TIME_ZONE = 'Europe/Zurich'

# Language code for this installation. All choices can be found here:
# http://www.i18nguy.com/unicode/language-identifiers.html
LANGUAGE_CODE = 'en-us'

SITE_ID = 1

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = False

# Absolute path to the directory that holds media.
# Example: "/home/media/media.lawrence.com/"
MEDIA_ROOT = os.path.join(PROJECT_PATH, 'public_html', 'media')

# URL that handles the media served from MEDIA_ROOT. Make sure to use a
# trailing slash if there is a path component (optional in other cases).
# Examples: "http://media.lawrence.com", "http://example.com/media/"
MEDIA_URL = ''

# URL prefix for admin media -- CSS, JavaScript and images. Make sure to use a
# trailing slash.
# Examples: "http://foo.com/media/", "/media/".
STATIC_URL = '/admin-media/'

# Make this unique, and don't share it with anybody.
SECRET_KEY = 'this-value-should-be-in-local_settings'

# List of callables that know how to import templates from various sources.
TEMPLATE_LOADERS = (
    'django.template.loaders.filesystem.Loader',
    'django.template.loaders.app_directories.Loader',

#     'django.template.loaders.eggs.load_template_source',
)

TEMPLATE_CONTEXT_PROCESSORS = (
    'django.contrib.auth.context_processors.auth',
    'django.core.context_processors.debug',
    'django.core.context_processors.i18n',
    'django.core.context_processors.media',
#    'django.core.context_processors.request',
)

"""
("django.contrib.auth.context_processors.auth",
"django.core.context_processors.debug",
"django.core.context_processors.i18n",
"django.core.context_processors.media",
"django.core.context_processors.static",
"django.contrib.messages.context_processors.messages")
"""







MIDDLEWARE_CLASSES = (
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',

    'problemdb.middleware.ErrorLogger'
)

AUTHENTICATION_BACKENDS = (
    'cernauth.backends.soapws.WSAuthBackend',
    'problemdb.auth.ActiveDirectoryBackend',
    'django.contrib.auth.backends.ModelBackend',
)

ROOT_URLCONF = 'lhcb_problemdb.urls'

TEMPLATE_DIRS = (
    # Put strings here, like "/home/html/django_templates" or "C:/www/django/templates".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(PROJECT_PATH, 'templates')
)

INSTALLED_APPS = (
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.sites',
    'django.contrib.admin',
    #'django.contrib.humanize',
    'lhcb_problemdb.problemdb',
    'cernauth'
)

LOGIN_REDIRECT_URL = "/"

# LDAP server settings. Use local_settings for the actual values
AD_DNS_NAME = None
AD_LDAP_PORT = None
AD_DNS_NAME = None
AD_NT4_DOMAIN = None
AD_SEARCH_DN = None


# RunDb integration. URIs used for searching the runs affected by a problem 
RUNDB_URI_RUN = 'http://lbrundb.cern.ch/rundb/run/'
RUNDB_URI_API_SEARCH = '//lbrundb.cern.ch/api/search/?'
RUNDB_URI_API_RUN = '//lbrundb.cern.ch/api/run/'


# CERN login integration
#
# Flag for enabling CERN single sign on login integration
# The application is running two different versions, one with ADFS disabled
# because the Shibboleth client is not compatible with the Apache version
# installed
ADFS_ENABLED = False

#
# URL of the web site with ADFS enabled authentication
ADFS_FALLBACK_URL = None


# CERN authentication web services
# Change these variables to enable it
CERN_WSAUTH_URI = None
CERN_WSAUTH_USER = None
CERN_WSAUTH_PASSWORD = None

CERN_WSAUTH_CLI_CMD = []
CERN_WSAUTH_CLI_PATH = ''
CERN_WSAUTH_CLI_SETTINGS = ''


#
# Define additional settings in the module local_settings.
# This should be used to override any options previously defined in this file.
#
try:
    from local_settings import *
except ImportError:
    print 'Warning, there are no local settings!!'
    raise


# Check that the database connection is defined
if not DATABASES:
    raise Exception("Database settings not defined. Check the local_settings module.")
