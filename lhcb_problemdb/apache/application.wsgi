#!/usr/bin/python
import os
import sys

os.environ['PYTHON_EGG_CACHE'] = '/tmp'

sys.path += ['/web/sites/problemdb/lib','/web/sites/problemdb/releases/June-2011', '/web/sites/problemdb/releases/June-2011/lhcb_problemdb','/web/sites/problemdb/releases/June-2011/lib/python2.4/site-packages']

os.environ['DJANGO_SETTINGS_MODULE'] = 'settings'
    
import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()

