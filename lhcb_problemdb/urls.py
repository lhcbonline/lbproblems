# -*- coding: utf-8 -*-

from django.conf.urls.defaults import *
from django.contrib import admin

from django.conf import settings

from django.contrib.auth.views import login, logout

admin.autodiscover()

urlpatterns = patterns('',
    (r'', include('problemdb.urls')),

    (r'^admin/', include(admin.site.urls)),

    url(r'^accounts/login/$',  login, dict(template_name="accounts/login.html"), name="accounts_login"),
    url(r'^accounts/logout/$', logout, dict(next_page="/"), name="accounts_logout"),

)

# Urls available only for development
if settings.DEBUG:

    #from django.contrib.auth.views import login, logout

    #url(r'^accounts/login/$',  login, dict(template_name="accounts/login.html"), name="accounts_login"),
    #url(r'^accounts/logout/$', logout, dict(next_page="/"), name="accounts_logout"),
    
    # Serve static files iwth Django only in DEBUG mode.
    urlpatterns += patterns('',
        (r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
            'show_indexes': True
        }),
    )

