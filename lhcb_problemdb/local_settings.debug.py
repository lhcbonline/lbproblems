# -*- coding: utf-8 -*-
#
#
#

import logging

logging.basicConfig()


from django.conf import settings

settings.MIDDLEWARE_CLASSES = list(settings.MIDDLEWARE_CLASSES)

settings.MIDDLEWARE_CLASSES.extend([
    'debug_toolbar.middleware.DebugToolbarMiddleware',
    ])

settings.INSTALLED_APPS = list(settings.INSTALLED_APPS)
settings.INSTALLED_APPS.extend([
    # Data generator. Added only for development
    #'south',
    'dilla',
    'debug_toolbar',
    ])


CERN_WSAUTH_URI = 'https://winservices-soap.web.cern.ch/winservices-soap/Generic/Authentication.asmx?WSDL'
CERN_WSAUTH_USER = 'webauth'
CERN_WSAUTH_PASSWORD = 'LHCb!soapJ9'
CERN_WSAUTH_LISTS_ALLOWED = ('lhcb-general',)

AD_DNS_NAME = 'localhost'
AD_LDAP_PORT = 3890



#DEBUG_PDB = False
#DEBUG_PDB = True

#if DEBUG_PDB:
    # Enable PDB
    # pass


# Use the Adfs authentication mock
ADFS_ENABLED = False

if ADFS_ENABLED:
    adfs_middleware = 'adfs.middleware.AdfsAuthenticationMiddleware'
    if adfs_middleware in settings.MIDDLEWARE_CLASSES:
        settings.INSTALLED_APPS.append('adfs.mock')

        idx = settings.MIDDLEWARE_CLASSES.index(adfs_middleware)
        settings.MIDDLEWARE_CLASSES.insert(idx, 'adfs.mock.middleware.MockAdfsAuthenticationMiddleware')


    ADFS_FALLBACK_URL = 'http://localhost:8000/accounts/login/'
