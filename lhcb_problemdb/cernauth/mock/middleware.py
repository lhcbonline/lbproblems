"""
"""
from django.conf import settings

from cern.authentication import tests
from cern.authentication import DEFAULT_SESSION_ADFS_USER

class MockAdfsAuthenticationMiddleware(object):
    """Middleware to mock the ADFS authentication based on Shibboleth.

    The middleware creates a mock authenticated user by adding to the 
    environment the variables required by the real ADFs authentication
    middleware.

    This class has to be included in the MIDDLEWARE_CLASSES list after that
    the SessionMiddleware and before the ADFS middleware.

    This class is meant be used when the developer is too lazy to configure a
    development environment with Shibboleth. 
    """

    def process_request(self, request):
        """
        Process the request and add the Shibboleth environment variables if
        the special GET argument `__mock_adfs` is present.

        """
        assert hasattr(request, 'session'), "The Django authentication middleware requires session middleware to be installed. Edit your MIDDLEWARE_CLASSES setting to insert 'django.contrib.sessions.middleware.SessionMiddleware'."

        SESSION_ADFS_USER  = getattr(settings, 'SESSION_ADFS_USER', DEFAULT_SESSION_ADFS_USER)

        # Use the current session if there is one
        user = request.session.get(SESSION_ADFS_USER)
        if user:
            login = user.get_login().replace("username_mock_", "")
            env = tests.get_mock_environ(login)
            request.META.update(env)

        return None
