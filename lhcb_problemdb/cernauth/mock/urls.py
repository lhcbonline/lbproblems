# -*- coding: utf-8 -*-
from django.conf.urls.defaults import patterns

import views

urlpatterns = patterns('',
    (r'^login/$',views.login),
    (r'^logout/$',views.logout),
    (r'^info/$',views.info),
)
