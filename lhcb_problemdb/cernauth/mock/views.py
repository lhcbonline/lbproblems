# -*- coding: utf-8 -*-
"""

"""

import os
import pprint

from django.http import HttpResponse

from cern.authentication import tests
from django.contrib import auth
from adfs.middleware import AdfsAuthenticationMiddleware

def login(request):
    """
    
    """
    env = tests.get_mock_environ("foo")
    request.META.update(env) 
    username = request.META[AdfsAuthenticationMiddleware.header]
 
    user = auth.authenticate(remote_user=username, env=request.META)
    if user:
        # User is valid.  Set request.user and persist user in the session
        # by logging the user in.
        request.user = user
        auth.login(request, user)
 
    #TODO: Send a face response
    return HttpResponse(content_type="text/plain")

def logout(request):
    #TODO: Implement fake logout
    return HttpResponse()

def info(request):
    response = HttpResponse(content_type="text/plain")
    
    response.write("\nUser\n")
    pprint.pprint(request.user, response)
    
    response.write("\nMETA\n")
    pprint.pprint(request.META, response)

    response.write("\nos.environ\n")
    pprint.pprint(os.environ, response)

    return response 
