"""
Module for integrate Django authentication with Shibboleth.

This module provides three classes:

- AdfsAuthenticationMiddleware:
  Middleware that checks for the presence of the variables set by Shibboleth

- AdfsUserBackend:
  Class that creates the AdfsUser object after a successful authentication  
    
- AdfsUser (in models.py):
  An extension to the Django User class that adds additional attributes to it.

This code is a modified version of the middleware classes provided by Django
for authenticating using the REMOTE_USER variable.  

    http://docs.djangoproject.com/en/1.1/howto/auth-remote-user/

"""


# Default values for the configuration settings
DEFAULT_META_ADFS_BUILDING = 'ADFS_BUILDING'
DEFAULT_META_ADFS_DEPARTMENT = 'ADFS_DEPARTMENT'
DEFAULT_META_ADFS_EMAIL = 'ADFS_EMAIL'
DEFAULT_META_ADFS_FIRSTNAME = 'ADFS_FIRSTNAME'
DEFAULT_META_ADFS_FULLNAME = 'ADFS_FULLNAME'
DEFAULT_META_ADFS_GROUP = 'ADFS_GROUP'
DEFAULT_META_ADFS_HOMEINSTITUTE = 'ADFS_HOMEINSTITUTE'
DEFAULT_META_ADFS_LASTNAME = 'ADFS_LASTNAME'
DEFAULT_META_ADFS_USERNAME = 'ADFS_LOGIN'
DEFAULT_META_ADFS_PERSONID = 'ADFS_PERSONID'
DEFAULT_META_ADFS_PHONENUMBER = 'ADFS_PHONENUMBER'

DEFAULT_SESSION_ADFS_USER = '_auth_adfs_user'

#DEFAULT_ADFS_REDIRECT_FIELD_NAME = 'next'
#DEFAULT_ADFS_LOGIN_REDIRECT_URL = '/profile'
DEFAULT_ADFS_LOGOUT_REDIRECT_URL = '/goodbye'
DEFAULT_ADFS_AUTH_FAIL_REDIRECT_URL = '/login'
DEFAULT_ADFS_PERMISSION_FAIL_REDIRECT_URL = '/fail'
DEFAULT_SHIBBOLETH_LOGIN_URL = '/Shibboleth.sso/?target='
DEFAULT_SHIBBOLETH_LOGOUT_URL='https://login.cern.ch/adfs/ls/?wa=wsignout1.0&returnurl='

