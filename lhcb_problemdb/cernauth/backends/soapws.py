"""
Backend for handling authentication using the CERN SOAP web services

Requirements:

 - Suds: Python client for consuming SOAP web services
   https://fedorahosted.org/suds/


This module must be configured with the following settings:

 CERN_WSAUTH_URI
   URI of the WSDL service description

 CERN_WSAUTH_USER
   Username of the service account allowed to access the web services.

 CERN_WSAUTH_PASSWORD
   Password of the service account allowed to access the web services.

 CERN_WSAUTH_LISTS_ALLOWED (Optional)
   Name of the allowed mailing list member the user can be member of in
   order to be authorized. The user has to be a member of at least one
   of them.

 CERN_WSAUTH_LISTS_REQUIRED (Optional)
   Names of the mailing lists the user must be member of in order to be
   authorized. The user has to be a member of all of them


For more details about the CERN SOAP web services, please refer to:

https://espace.cern.ch/authentication/CERN%20Authentication%20Help/SOAP%20WebServices.aspx


"""

from django.conf import settings
from django.contrib.auth.models import User

from suds.client import Client

from cernauth.models import AdfsUser

class WSAuth(object):
    """
    Wrapper class for using CERN Authentication SOAP WebServices

    Currently, the only method implemented is:
        - GetUserInfo

    """
    # Response code for successful authentication
    AUTH_SUCCESSFUL = 3

    def __init__(self, uri, username, password):
        """
        Initializes the client.

        `uri`: URI of the WSDL service description
        `username` and `password` are the required credentials to use the web
        services.
        """
        self.uri = uri
        self.client = Client(uri, username=username, password=password)

    def authenticate(self, username, password):
        """
        Authenticates a user using the username and password. The user name
        can be the NICE login or an email address.

        Returns a response object if the authentication was sucessful, otherwise
        returns None
        """
        user = self.client.service.GetUserInfo(username, password)
        if user.auth == self.AUTH_SUCCESSFUL:
            return user

        return None

    def is_member_of_list(self, username, listname):
        return self.client.service.UserIsMemberOfList(username, listname)


class WSAuthBackend(object):
    """
    A Django Authentication Backend that uses the CERN SOAP web services.
    """
    # Create a User object if not already in the database?
    create_unknown_user = True

    wsauth = WSAuth(settings.CERN_WSAUTH_URI,
                        settings.CERN_WSAUTH_USER,
                        settings.CERN_WSAUTH_PASSWORD)

    def authenticate(self, username, password):
        """
        Authenticates a user using the username and password.

        If the setting CERN_WSAUTH_LISTS_REQUIRED is defined, the authenticated
        user must be a member of every list included. Otherwise, it would not be
        authenticated.

        If the setting CERN_WSAUTH_LISTS_ALLOWED is defined, the authenticated
        user must be a member of one of the lists included. Otherwise, it would not be
        authenticated.
        """
        wsresponse = self.wsauth.authenticate(username, password)
        if not wsresponse:
            return None


        required_lists = getattr(settings, "CERN_WSAUTH_LISTS_REQUIRED", [])
        for listname in required_lists:
            if not self.wsauth.is_member_of_list(username, listname):
                return None

        allowed_lists = getattr(settings, "CERN_WSAUTH_LISTS_ALLOWED", [])
        in_allowed_lists = len(allowed_lists) == 0
        for listname in allowed_lists:
            if self.wsauth.is_member_of_list(username, listname):
                in_allowed_lists = True

        if not in_allowed_lists:
            return None


        user = None

        # Note that this could be accomplished in one try-except clause, but
        # instead we use get_or_create when creating unknown users since it has
        # built-in safeguards for multiple threads.
        if self.create_unknown_user:
            user, created = AdfsUser.objects.get_or_create(username=username)
            if created:
                user = self._configure_user(user, wsresponse)
        else:
            try:
                user = AdfsUser.objects.get(username=username)
            except User.DoesNotExist:
                return None

        user = self._configure_user(user, wsresponse)
        user.save()

        return user


    def _configure_user(self, user, wsresponse ):
        """
        Populates the newly created `user` object with the values retrieved
        from the web service
        """
        fields = (
            ('first_name', 'firstname'),
            ('last_name', 'lastname'),
            ('email', 'email'),
            ('personid', 'ccid'),
            ('department', 'department'),
            ('phonenumber', 'telephonenumber'),
            ('homeinstitute', 'company'),
        )

        for (attr, source) in fields:
            setattr(user, attr, getattr(wsresponse, source))

        return user


    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None


