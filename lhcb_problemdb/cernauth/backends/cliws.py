"""
Backend for handling authentication using a CLI for the CERN SOAP web services

This authentication backend is usefull when it is not possible to run the SOAP
client from the web application.

Requirements:

 - A script that queries the CERN authentication web services

 - Suds: Python client for consuming SOAP web services
   https://fedorahosted.org/suds/


This module must be configured with the following settings:

 CERN_WSAUTH_URI
   URI of the WSDL service description

 CERN_WSAUTH_USER
   Username of the service account allowed to access the web services.

 CERN_WSAUTH_PASSWORD
   Password of the service account allowed to access the web services.

 CERN_WSAUTH_LISTS_ALLOWED (Optional)
   Name of the allowed mailing list member the user can be member of in
   order to be authorized. The user has to be a member of at least one
   of them.

 CERN_WSAUTH_LISTS_REQUIRED (Optional)
   Names of the mailing lists the user must be member of in order to be
   authorized. The user has to be a member of all of them


 CERN_WSAUTH_CLI_CMD
   A sequence of the command line arguments used to start the CLI script.
   It has to be a sequence (list) with the format used by the subprocess
   module (the first element is the executable and the rest are the arguments)

 CERN_WSAUTH_CLI_PATH
   Entries to the PYTHONPATH variable (string)

 CERN_WSAUTH_CLI_SETTINGS
   The name of the settings module for Django


For more details about the CERN SOAP web services, please refer to:

https://espace.cern.ch/authentication/CERN%20Authentication%20Help/SOAP%20WebServices.aspx


"""

from django.conf import settings
from django.contrib.auth.models import User

from cernauth.models import AdfsUser

import subprocess
import pickle

class WSCliAuth(object):

    def __init__(self, cmd, python_path, django_settings):
        self.cmd = cmd
        self.env = {
            'PYTHONPATH': python_path,
            'DJANGO_SETTINGS_MODULE': django_settings
        }

    def _call(self, arg, env):
        cmd = list(self.cmd)
        cmd.append(arg)

        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE, env=env)
        (out, err) = proc.communicate()

        if out:
            out = pickle.loads(out)
        return (proc.returncode, out, err) 


    def authenticate(self, username, password):
        env = dict(self.env)
        env['cernauth_username'] = username
        env['cernauth_password'] = password
        
        (returncode, out, err) = self._call("authenticate", env)
 
        if returncode != 0:
            return None

        return out

    def is_member_of_list(self, username, listname):
        env = dict(self.env)
        env['cernauth_username'] = username
        env['cernauth_listname'] = listname

        (returncode, out, err) = self._call("is_member_of_list", env)
        if returncode != 0:
            return False
        
        return out



class CliWSAuthBackend(object):
    wsauth = WSCliAuth(settings.CERN_WSAUTH_CLI_CMD,
                settings.CERN_WSAUTH_CLI_PATH,
                settings.CERN_WSAUTH_CLI_SETTINGS)

    # Create a User object if not already in the database?
    create_unknown_user = True

    def authenticate(self, username, password):
        """
        Authenticates a user using the username and password.

        If the setting CERN_WSAUTH_LISTS_REQUIRED is defined, the authenticated
        user must be a member of every list included. Otherwise, it would not be
        authenticated.

        If the setting CERN_WSAUTH_LISTS_ALLOWED is defined, the authenticated
        user must be a member of one of the lists included. Otherwise, it would not be
        authenticated.
        """
        wsresponse = self.wsauth.authenticate(username, password)

        if not wsresponse:
            return None


        required_lists = getattr(settings, "CERN_WSAUTH_LISTS_REQUIRED", [])
        for listname in required_lists:
            if not self.wsauth.is_member_of_list(username, listname):
                return None

        allowed_lists = getattr(settings, "CERN_WSAUTH_LISTS_ALLOWED", [])
        in_allowed_lists = len(allowed_lists) == 0
        for listname in allowed_lists:
            if self.wsauth.is_member_of_list(username, listname):
                in_allowed_lists = True

        if not in_allowed_lists:
            return None


        user = None

        # Note that this could be accomplished in one try-except clause, but
        # instead we use get_or_create when creating unknown users since it has
        # built-in safeguards for multiple threads.
        if self.create_unknown_user:
            user, created = AdfsUser.objects.get_or_create(username=username)
            if created:
                user = self._configure_user(user, wsresponse)
        else:
            try:
                user = AdfsUser.objects.get(username=username)
            except User.DoesNotExist:
                return None

        user = self._configure_user(user, wsresponse)
        user.save()

        return user

    def get_user(self, user_id):
        try:
            return User.objects.get(pk=user_id)
        except User.DoesNotExist:
            return None


    def _configure_user(self, user, wsresponse):
        fields = (
            ('first_name', 'firstname'),
            ('last_name', 'lastname'),
            ('email', 'email'),
            ('personid', 'ccid'),
            ('department', 'department'),
            ('phonenumber', 'telephonenumber'),
            ('homeinstitute', 'company'),
        )

        for (attr, source) in fields:
            setattr(user, attr, wsresponse.get(source))

        return user

