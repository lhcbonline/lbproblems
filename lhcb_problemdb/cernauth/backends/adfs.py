
from django.contrib.auth.models import User
from django.contrib.auth.backends import ModelBackend 

from cernauth.models import AdfsUser

class AdfsUserBackend(ModelBackend):
    """
    This backend is to be used in conjunction with the AdfsAuthenticationMiddleware
    found in the middleware module of this package, and is used when the server
    is handling authentication outside of Django.

    By default, the ``authenticate`` method creates ``AdfsUser`` objects for
    usernames that don't already exist in the database.  Subclasses can disable
    this behavior by setting the ``create_unknown_user`` attribute to
    ``False``.

    Code taken from the class RemoteUserBackend distributed in Django.
    """

    # Create a User object if not already in the database?
    create_unknown_user = True

    def authenticate(self, remote_user, env=None):
        """
        The username passed as ``remote_user`` is considered trusted.  This
        method simply returns the ``User`` object with the given username,
        creating a new ``User`` object if ``create_unknown_user`` is ``True``.

        Returns None if ``create_unknown_user`` is ``False`` and a ``User``
        object with the given username is not found in the database.
        """
        if not remote_user:
            return
        user = None
        username = self.clean_username(remote_user)

        # Note that this could be accomplished in one try-except clause, but
        # instead we use get_or_create when creating unknown users since it has
        # built-in safeguards for multiple threads.
        if self.create_unknown_user:
            user, created = AdfsUser.objects.get_or_create(username=username)
            if created:
                user = self.configure_user(user, env)
        else:
            try:
                user = AdfsUser.objects.get(username=username)
            except User.DoesNotExist:
                pass
        return user

    def clean_username(self, username):
        """
        Performs any cleaning on the "username" prior to using it to get or
        create the user object.  Returns the cleaned username.

        By default, returns the username unchanged.
        """
        return username

    def configure_user(self, user, env):
        """
        Configures a user after creation and returns the updated user.

        Arguments are:

            `user`: The object AdfsUser created by the method `authenticate`
            `env`: A dictionary with the all the environment passed from the
                server. It contains the variables set by Shibboleth.

        """
        # List of attributes set to the User object
        fields = (
            ('first_name', 'META_ADFS_FIRSTNAME', DEFAULT_META_ADFS_FIRSTNAME),
            ('last_name', 'META_ADFS_FULLNAME', DEFAULT_META_ADFS_FULLNAME),
            ('email', 'META_ADFS_EMAIL', DEFAULT_META_ADFS_EMAIL),
            ('adfs_groups', 'META_ADFS_GROUP', DEFAULT_META_ADFS_GROUP),
        )

        for (attr, setting, default) in fields:
            setattr(user, attr, env.get(getattr(settings, setting, default)))

        return user

