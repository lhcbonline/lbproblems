
from urlparse import urlsplit

from django.http import HttpResponseRedirect
from django.utils.http import urlquote

from django.contrib.auth import REDIRECT_FIELD_NAME

from django.conf import settings

from cernauth import DEFAULT_SHIBBOLETH_LOGIN_URL,\
    DEFAULT_ADFS_LOGOUT_REDIRECT_URL,\
    DEFAULT_SHIBBOLETH_LOGOUT_URL


def login(request):
    """
    Possible request parameters (GET or POST):
    
    REDIRECT_FIELD_NAME: 
    referrer: Redirect to the referrer after logging in. 
    """
    SHIBBOLETH_LOGIN_URL = getattr(settings, 'SHIBBOLETH_LOGIN_URL', DEFAULT_SHIBBOLETH_LOGIN_URL)

    if request.user.is_authenticated():
        redirect_to = request.REQUEST.get(REDIRECT_FIELD_NAME, '')

        if not redirect_to:
            referrer = request.REQUEST.get('HTTP_REFERER', None)
            if referrer:
                try: 
                    redirect_to = urlsplit(referrer, 'http', False)[2] 
                except IndexError: 
                    pass 

        if not redirect_to or '//' in redirect_to or ' ' in redirect_to:
            redirect_to = settings.LOGIN_REDIRECT_URL

        return HttpResponseRedirect(redirect_to)

    else:
        path = urlquote(request.build_absolute_uri(request.get_full_path()))
        return HttpResponseRedirect('https://' + request.get_host() + SHIBBOLETH_LOGIN_URL + path)


def logout(request, next_page=None, redirect_field_name=REDIRECT_FIELD_NAME):
    # Close the session in Django
    from django.contrib.auth import logout
    logout(request)
 
    redirect_to = next_page
    if not redirect_to:
        redirect_to = request.REQUEST.get(redirect_field_name, '')

    if not redirect_to or '//' in redirect_to or ' ' in redirect_to:
        ADFS_LOGOUT_REDIRECT_URL = getattr(settings, 'ADFS_LOGOUT_REDIRECT_URL', DEFAULT_ADFS_LOGOUT_REDIRECT_URL)
        redirect_to = ADFS_LOGOUT_REDIRECT_URL

    # Redirect to Shibboleth logout URL
    SHIBBOLETH_LOGOUT_URL = getattr(settings, 'SHIBBOLETH_LOGOUT_URL', DEFAULT_SHIBBOLETH_LOGOUT_URL)
    path = urlquote(request.build_absolute_uri(redirect_to))
    return HttpResponseRedirect(SHIBBOLETH_LOGOUT_URL + '' + path)
