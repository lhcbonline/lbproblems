
from django.contrib.auth.models import User

class AdfsUser(User):
    """
    This class extends the Django standard `User` model and adds the extra
    attributes used by CERN SSO Authentication.
    
    AdfsUser is a *proxy* for the User model and thus doesn't affect the 
    behavior of the original class concerning database access. 
    
    Adding attributes is the preferred way for storing the additional
    information instead of a Profile module, for example. In this case, the
    users of this authentication backend can define their own profile module
    for each application.
    
    For more information about this, please refer to the Django documentation:

    http://docs.djangoproject.com/en/1.1/topics/db/models/#proxy-models
    http://docs.djangoproject.com/en/dev/topics/auth/#storing-additional-information-about-users
    """
    
    def __init__(self, *args, **kwargs):
        User.__init__(self, *args, **kwargs)
    
        self.adfs_groups = kwargs.get("groups", [])
        self.personid = kwargs.get("personid")
        self.department = kwargs.get("department")
        self.building = kwargs.get("building")
        self.phonenumber = kwargs.get("phonenumber")
        self.homeinstitute = kwargs.get("homeinstitute")
    
    class Meta:
        proxy = True

    def in_adfs_group(self, group):
        return group in self.adfs_groups
