#!/usr/bin/env python
#
# Command line interface for the CERN authentication SOAP services.
#
# Usage:
#
#  python get-user-info.py authentication|is_member_of_list
#
# The arguments are passed as environment variables.
#
# It should be used when the application can not call the SOAP web services 
# itself.
#

import sys
import os
import pickle

from django.conf import settings

from suds.client import Client

class WSAuth(object):
    """
    Wrapper class for using CERN Authentication SOAP WebServices

    Currently, the only method implemented is:
        - GetUserInfo

    """
    # Response code for successful authentication
    AUTH_SUCCESSFUL = 3

    def __init__(self, uri, username, password):
        """
        Initializes the client.

        `uri`: URI of the WSDL service description
        `username` and `password` are the required credentials to use the web
        services.
        """
        self.uri = uri
        self.client = Client(uri, username=username, password=password)

    def authenticate(self, username, password):
        """
        Authenticates a user using the username and password. The user name
        can be the NICE login or an email address.

        Returns a response object if the authentication was sucessful, otherwise
        returns None
        """
        user = self.client.service.GetUserInfo(username, password)
        if user.auth == self.AUTH_SUCCESSFUL:
            return user

        return None

    def is_member_of_list(self, username, listname):
        return self.client.service.UserIsMemberOfList(username, listname)



def authenticate(username, password):
    wsauth = WSAuth(settings.CERN_WSAUTH_URI, 
        settings.CERN_WSAUTH_USER, settings.CERN_WSAUTH_PASSWORD)


    user = wsauth.authenticate(username, password)

    if user == None:
        return 1


    fields = ('firstname', 'lastname', 'email', 'ccid', 'department', 'telephonenumber', 'company')

    out = dict([(f, unicode(getattr(user, f))) for f in fields])
    pickle.dump(out, sys.stdout)
    return 0

def is_member_of_list(username, listname):
    wsauth = WSAuth(settings.CERN_WSAUTH_URI,
        settings.CERN_WSAUTH_USER, settings.CERN_WSAUTH_PASSWORD)

    rtn = wsauth.is_member_of_list(username, listname)
    pickle.dump(rtn, sys.stdout)

    return 0

if __name__ == '__main__':

    username = os.getenv('cernauth_username')

    fn = sys.argv[1]

    if fn == "authenticate":
        password = os.getenv('cernauth_password')
        if not username or not password:
            print >> sys.stderr, "Provide user credentials in enviroment variables: 'cernauth_username' and 'cernauth_password'"
            sys.exit(1)

        rtn = authenticate(username, password)
    elif fn == "is_member_of_list":
        listname = os.getenv('cernauth_listname')
        rtn = is_member_of_list(username, listname)

    sys.exit(rtn)


