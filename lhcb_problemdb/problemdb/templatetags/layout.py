# -*- coding: utf-8 -*-
"""
Template tag and filter module for showing various elements of the problem
database.

"""
from django import template

register = template.Library()


@register.inclusion_tag('problemdb/problem_table.html')
def problem_table(problems):
    """
    """
    return {'problems' : problems}

@register.filter
def tablecols(data, cols):
    """
    Generic filter that breaks data to be displayed as a table with a certain
    number of columns columns.

    The tag creates a list of rows, each one containing `cols` elements to be
    placed on the cells of a table. For example:

        <table>
        {% for row in members|tablecols:5 %}
            <tr>
            {% for member in row %}
                <td>
                    {% show_simple_profile member user %}
                </td>
            {% endfor %}
            </tr>
        {% endfor %}
        </table>

    Code originally written by:
    http://diffract.me/2009/09/django-template-filter-show-list-of-objects-as-table-with-fixed-number-of-columns/

    """
    rows = []
    row = []
    index = 0
    for user in data:
        row.append(user)
        index = index + 1
        if index % cols == 0:
            rows.append(row)
            row = []
    # Still stuff missing?
    if len(row) > 0:
        rows.append(row)
    return rows

#register.filter_function(tablecols)