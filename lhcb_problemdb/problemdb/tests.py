"""
This file demonstrates two different styles of tests (one doctest and one
unittest). These will both pass when you run "manage.py test".

Replace these with more appropriate tests for your application.
"""

from django.test import TestCase

import models

class Api(TestCase):
    fixtures = ['apikey', 'systems', 'severities']

    def setUp(self):
        self.valid_data = {
            'title': 'title',
            'initial_comment': 'initial_comment',
            'author_name' : 'test-author',
            'link' : 'http://localhost/'
        }

        apikey = models.ApiKey.objects.all()[0]        
        self.valid_data['apikey'] = apikey.value

        system = models.System.objects.all()[0]
        self.valid_data['system_name'] = system.name

    def test_create_noapi(self):
        data = {'foo': '123'}
        response = self.client.post('/api/problems/', data)
        self.failUnlessEqual(response.status_code, 403)

    def test_create_systemname(self):
        data = {}
        data.update(self.valid_data)
        data['system_name'] = '----non-existing'
        
        response = self.client.post('/api/problems/', data)
        self.failUnlessEqual(response.status_code, 404)
        
    def test_create_ok(self):
        data = {}
        data.update(self.valid_data)
        #print data

        response = self.client.post('/api/problems/', data)
        self.failUnlessEqual(response.status_code, 201)
        if not response['Location']:
            self.fail("No URL returned for the new object")

        self.failUnlessEqual(models.Problem.objects.all().count(), 1)
        self.failUnlessEqual(models.Comment.objects.all().count(), 1)

        p = models.Problem.objects.all()[0]
        self.failUnless(len(p.comments.all()) == 1, "No comments created")
        

        

