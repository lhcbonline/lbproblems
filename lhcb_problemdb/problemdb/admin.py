# -*- coding: utf-8 -*-
"""
Admin settings
"""

from django.contrib import admin
 
from problemdb.models import Problem, Comment, System, Severity, ApiKey

class CommentInline(admin.TabularInline):
    model = Comment

class ProblemAdmin(admin.ModelAdmin):
    list_display = ("id", "title", "severity", "system", "added_at", "started_at", "ended_at",)
    date_hierarchy = 'started_at'
    list_filter = ("severity", "system", "ended_at", )

    inlines = (
        CommentInline,
    )

class SystemAdmin(admin.ModelAdmin):
    list_display = ("name", "ordering")
    list_editable = ("ordering", )

    ordering = ("-ordering", "name")

class SeverityAdmin(admin.ModelAdmin):
    list_display = ("name", "assessed", "ordering")
    ordering = ("-ordering", "name")
    list_editable = ("ordering", )

admin.site.register(System, SystemAdmin)
admin.site.register(Severity, SeverityAdmin)
admin.site.register(Problem, ProblemAdmin)
admin.site.register(ApiKey)
