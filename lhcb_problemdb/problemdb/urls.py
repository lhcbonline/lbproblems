# -*- coding: utf-8 -*-

from django.conf.urls.defaults import patterns, url

from django.views.generic.simple import redirect_to

import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),

    url(r'^report/$', redirect_to, {'url': '/report/active/'}),
    url(r'^report/active/$', views.problems_print),

    url(r'^problems/new/$', views.problem_new, name='problem_new'),

    url(r'^search/run/$', views.problem_search_run, name='problem_search_run'),

    url(r'^search/$', views.problem_search, name='problem_search'),

    url(r'^problems/(?P<problem_id>\d+)/$', views.problem, name='problem_detail'),

    url(r'^problems/(?P<problem_id>\d+)/undo/$', views.problem_undo, name='problem_undo'),

    url(r'^problems/(?P<problem_id>\d+)/edit/$', views.problem_edit, name='problem_edit'),
    
    url(r'^problems/(?P<problem_id>\d+)/close/$', views.problem_close, name='problem_close'),

    url(r'^comments/(?P<problem_id>\d+)/$', views.comments, name='comments'),
    
    url(r'api/search/$', views.api_search),
    url(r'api/systems/$', views.api_systems),
    url(r'api/problems/$', views.api_create_problem),
    url(r'api/activity/$', views.api_activity)
)
