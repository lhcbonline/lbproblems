"""
A context processor that adds to the template context the variables needed
to handle the authentication via ADFS.

The context processor looks for the following setting variables:

 - ADFS_ENABLED
 - ADFS_FALLBACK_URL

If they are not found, a None value will be used.

"""

from django.conf import settings

def adfs(request):
    """
    Adds ADFS authentication context variables to the template context.
    """
    return {
        'ADFS_ENABLED': getattr(settings, 'ADFS_ENABLED', False)
        'ADFS_FALLBACK_URL': getattr(settings, 'ADFS_FALLBACK_URL', None)
    }
