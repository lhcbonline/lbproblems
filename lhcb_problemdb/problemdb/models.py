# -*- coding: utf-8 -*-
import datetime

from django.db import models
from django.contrib.auth.models import User

class Severity(models.Model):
    name = models.CharField(max_length=100)
    description = models.TextField(null=True, blank=True)
    ordering = models.IntegerField(default=0, help_text="Items with a higher value appear first in the lists. Use a negative number to move an item to the last position.")
    # TODO: Add an attribute for specifying that the severity should be reported

    assessed = models.BooleanField(default=False, help_text="Problems with this severity can be closed")

    def __unicode__(self):
        return self.name

    class Meta:
        verbose_name_plural = "severities"
        ordering = ("-ordering", "name")

class System(models.Model):
    name = models.CharField(max_length=100)
    #alias = models.SlugField()
    description = models.TextField(null=True, blank=True)
    ordering = models.IntegerField(default=0, help_text="Items with a higher value appear first in the lists. Use a negative number to move an item to the last position.")
    visible = models.BooleanField(default=True, help_text="Display this system on the home page")

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ("-ordering", "name")


class Problem(models.Model):
    """
    A record of a problem
    
    The author can be null when the problem is created by a non-authenticated
    user.
    """
    author = models.ForeignKey(User, related_name="problems", null=True, blank=True)
    system = models.ForeignKey(System, related_name="problems")
    severity = models.ForeignKey(Severity, related_name="problems")

    added_at = models.DateTimeField(default=datetime.datetime.now)
    started_at = models.DateTimeField(default=datetime.datetime.now)
    ended_at = models.DateTimeField(null=True, blank=True)

    title = models.CharField(max_length=300)

    author_name = models.CharField(max_length=300, null=True, blank=True, 
        help_text="Name of the author used when the problem is created by an unauthenticated user.")

    def get_author_full_name(self):
        return self.author  and self.author.get_full_name() or self.author_name

    def __unicode__(self):
        return self.title

    @models.permalink
    def get_absolute_url(self):
        return ('problem_detail', [str(self.id)])

    def is_assessed(self):
        """
        States if a problem has been assessed and an end date can be set.

        A problem is assessed when it has a severity that has the `assessed`
        attribute.
        """
        return self.severity.assessed

    def save(self, *args, **kwargs):
        """
        Validates the following restrictions before saving the problem:
        
         - The severity must be assessed before closing
         - The author name must be specified if the author is null.
        """
        
        if not self.author and not self.author_name:
            raise Exception("The author name must be used for problems from unauthenticated users")

        if not self.is_assessed() and self.ended_at:
            raise Exception("End date can be set only once the severity has been assessed")

        if self.ended_at and self.ended_at < self.started_at:
            raise Exception("The end date must be after the start date of the problem")

        super(Problem, self).save(*args, **kwargs)

    def close(self, ended_at):
        """
        Closes the problem setting its end date.
        """
        self.ended_at = ended_at

    def report_changes(self, user, changed_data):
        """
        Creates a comment with the report of the changes made by an user.

        Finds the differences between the current model data and the 
        argument `changed_data`.

        user: A models.User instance
        changed_data: A dictionary with the changed fields (keys) and the
              modified data (values)
        """
        if not changed_data:
            return

        field_name = lambda s: s.title().replace("_", " ")

        message = "Updated the problem report and changed %d fields (%s)" % (
            len(changed_data),
            ", ".join([field_name(field) for field in changed_data])
        )

        message_detail = []

        # Report the detailed changes for the following fields
        detailed_fields = ("severity", "ended_at")
        for field in detailed_fields:
            if field in changed_data:

                text = "'%s' changed from '%s' to '%s'" % (field_name(field),
                    changed_data[field][0] or "(empty)",
                    changed_data[field][1] or "(empty)")

                message_detail.append(text)

        comment = Comment()
        comment.problem = self
        comment.author = user
        comment.comment = "%s.\n%s" % (message, ". ".join(message_detail))
        comment.save()


class Comment(models.Model):
    problem = models.ForeignKey(Problem, related_name="comments")
    author = models.ForeignKey(User, related_name="comments", null=True, blank=True)
    added_at = models.DateTimeField(default=datetime.datetime.now)
    comment = models.TextField()
    link = models.URLField(null=True, blank=True, verify_exists=False)
    
    author_name = models.CharField(max_length=300, null=True, blank=True, 
        help_text="Name of the author used when the problem is created by an unauthenticated user.")

    def get_author_full_name(self):
        return self.author  and self.author.get_full_name() or self.author_name

    def save(self, *args, **kwargs):
        """
        Validates the following restrictions before saving the problem:
        
         - The severity must be assessed before closing
         - The author name must be specified if the author is null.
        """
        
        if not self.author and not self.author_name:
            raise Exception("The author name must be used for problems from unauthenticated users")

        super(Comment, self).save(*args, **kwargs)

    class Meta:
        ordering = ["added_at"]


class ApiKey(models.Model):
    """
    An ApiKey is the password that external applications use to access
    the problem database. 
    """
    value = models.CharField(max_length=200)
