#
"""
Middleware classes
"""

import datetime
import traceback

from django.conf import settings

class ErrorLogger(object):
    """
    Write a log of the exceptions in the file defined by settings.ERROR_LOG_FILE.
    It does not do anything if the setting is not defined.

    """
    def process_exception(self, request, exception):

        filename = getattr(settings, 'ERROR_LOG_FILE', None)

        if not filename:
            return

        try:
            log = open(filename, 'a+')

            tb_text = traceback.format_exc()
            url = request.build_absolute_uri()

            now = datetime.datetime.now()
            time = now.strftime("%c")

            log.write(time + ' ' + url + '\n' + str(tb_text) + '\n\n')
            log.close()

        except:
            pass

