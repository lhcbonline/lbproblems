# -*- coding: utf-8 -*-

import csv
import datetime

from django import http
from django.template import RequestContext
from django.shortcuts import render_to_response, get_object_or_404, redirect
from django.core.urlresolvers import reverse
from django.contrib.auth.decorators import login_required
from django.db.models import Count, Q

from django.conf import settings

from models import Problem, System, Severity, Comment, ApiKey
from forms import ProblemForm, ProblemCreateForm, CommentForm, ProblemCloseForm, ApiProblemCreateForm
from django.utils import simplejson

def index(request):
    # TODO: Temporary disabled. It looks like its a bug in Django's Oracle backend
    #systems = System.objects.order_by('name').annotate(num_problems=Count('problems'))
    systems = System.objects.all()
    active_problems = Problem.objects.filter(ended_at__isnull=True, system__visible__exact=True).exclude(severity__name='Report').order_by('-started_at').select_related()
    recent_problems = Problem.objects.filter(system__visible__exact=True).order_by('-added_at').all()[:10]
    reports         = Problem.objects.filter(ended_at__isnull=True, system__visible__exact=True).filter(severity__name='Report').order_by('-added_at').all()

    return render_to_response('problemdb/index.html', {
        'recent_problems' : recent_problems,
        'active_problems' : active_problems,
        'reports'         : reports,
        'systems' : systems,
        
    }, context_instance=RequestContext(request))


def _search(request):
    """
    Performs a search based on the arguments passed on a request.
    
    Returns a tuple with two elements:
    
    - A dictionary with the filters used
    - The search results 
    """
    
    # Identity dummy function
    foo = lambda s: s
    
    # Table that matches query arguments and the object attributes used for the
    # filters.
    #
    # It's a list of 3-tuples with the following structure:
    # - Name of the query string argument
    # - Name of the model attribute used for the query filters.
    # - Function to transform the value received in the query string to the 
    #   one expected by the query filters. Eg. transform a text formatted date
    #   to a datetime object.
    #
    possible_arguments = (
        ('system', 'system', lambda id: System.objects.get(pk=id)),
        ('severity', 'severity', lambda id: Severity.objects.get(pk=id)),

        ('system_name', 'system', lambda name: System.objects.get(name=name)),

        # TODO: Add functions for parsing dates
        ('started_gte', 'started_at__gte', foo),
        ('started_gt', 'started_at__gt', foo),
        ('started_lt', 'started_at__lt', foo),
        ('started_lte', 'started_at__lte', foo),
        ('closed_gte', 'ended_at__gte', foo),
        ('closed_gt', 'ended_at__gt', foo),
        ('closed_lt', 'ended_at__lt', foo),
        ('closed_lte', 'ended_at__lte', foo),
        ('added_gt', 'added_at__lt', foo),
        ('added_lt', 'added_at__gt', foo),
        ('system_visible', 'system__visible__exact', foo),
        
        # TODO: Search ranges: 
        # start < enddate &  ended > startdate
        #
    )


    # 
    filters = {}
    for (arg_name, filter_name, transform_func) in possible_arguments:
        arg_value = request.GET.get(arg_name)

        if arg_value:
            filters[filter_name] = transform_func(arg_value)


    problems = Problem.objects.select_related().filter(**filters)

    # Additional filters

    # Get the problems that are still open or that ended after a certain date.
    open_or_closed_gte = request.GET.get('open_or_closed_gte')
    if open_or_closed_gte:
        problems = problems.filter((Q(ended_at__gt = open_or_closed_gte) | Q(ended_at__isnull=True)))

    # Sorting field
    sort_field = request.GET.get('sort_field') or "started_at"
    problems = problems.order_by(sort_field)

    # basic keyword search
    q = request.GET.get('q')
    if q:
        problems = problems.filter(title__icontains=q)
        filters['q'] = q
    
    return (filters, problems)


def problem_search(request):
    (filters, problems) = _search(request)
    
    #
    inline = request.GET.get('_inline')
    
    template = 'problemdb/search.html'
    if inline:
        template = 'problemdb/problem_table.html'
    
    return render_to_response(template, {
        'problems': problems,
        'filters' : filters,
    }, context_instance=RequestContext(request))

def problem_search_run(request, ):
    """
    Search by run number
    
    This is done in 3 steps:
    
    1. A query string argument is needed (run_id) to pass it to the template
       that will call the Javascript APIs of the RunDb and the Problemdb itself.
       
    2. Get the 
    
    """
    run_id = request.GET['run_id'] 
    
    return render_to_response('problemdb/search_run.html', {
        'run_id' : run_id,
        
        'RUNDB_URI_RUN': settings.RUNDB_URI_RUN,
        'RUNDB_URI_API_RUN': settings.RUNDB_URI_API_RUN,
        'RUNDB_URI_API_SEARCH': settings.RUNDB_URI_API_SEARCH,
        
    }, context_instance=RequestContext(request))


def problem(request, problem_id):
    """
    Displays the information of a problem and its comments.
    """
    p = get_object_or_404(Problem, pk=problem_id)
    return render_to_response('problemdb/problem_detail.html', {
        'problem': p,
        'comments': p.comments.all(),
        'comment_form': CommentForm(),
        'RUNDB_URI_RUN': settings.RUNDB_URI_RUN,
        'RUNDB_URI_API_SEARCH': settings.RUNDB_URI_API_SEARCH,
    }, context_instance=RequestContext(request))


@login_required
def problem_new(request):
    """
    Create a new problem.
    """
    if request.method == 'POST':
        form = ProblemCreateForm(request.POST)
        if form.is_valid():
            problem = form.save(commit=False)
            problem.author = request.user
            problem.save()

            comment = Comment()
            comment.problem = problem
            comment.author = request.user
            comment.comment = form.cleaned_data["initial_comment"]
            comment.link = form.cleaned_data.get("link")
            comment.save()

            uri = reverse('problem_detail', kwargs={'problem_id':problem.id})
            uri += '?' + http.urlencode({'new-problem': problem.id})
            return redirect(uri)

    else:
        # TODO: How to find the default value for the severity of a new problem?
        default_severity = Severity.objects.order_by('-ordering')[0]
        form = ProblemCreateForm(initial={'severity': default_severity.id})

    # Render the form if the request was a GET or if the data was not valid
    return render_to_response('problemdb/problem_form.html', {
        'form': form,
        'message_title' : 'Report a new problem',
    }, context_instance=RequestContext(request))

@login_required
def problem_edit(request, problem_id):
    problem = get_object_or_404(Problem, pk=problem_id)
    if request.method == 'POST':
        form = ProblemForm(request.POST, instance=problem)

        changed_data = {}
        for field in form.changed_data:
            changed_data[field] = [getattr(problem,field),None]

        if form.is_valid():

            # Record the changes made to the problem_detail
            #changed_data = {}
            for field in form.changed_data:
                changed_data[field][1] = form.cleaned_data[field]

            problem.report_changes(request.user, changed_data)

            #problem = form.save(commit=False)
            form.save(commit=False)
            #problem.id = problem_id
            problem.save()
            uri = reverse('problem_detail', kwargs={'problem_id':problem.id})
            uri += '?' + http.urlencode({'update-problem': problem.id})
            return redirect(uri)

    else:
        form = ProblemForm(instance=problem)


    # Render the form if the request was a GET or if the data was not valid
    return render_to_response('problemdb/problem_form.html', {
        'form': form,
        'problem': problem,
        'message_title' : 'Edit problem report',
    }, context_instance=RequestContext(request))

@login_required
def problem_undo(request, problem_id):

    #TODO: Check if it's possible to delete the problem.

    return redirect('/')

@login_required
def problem_close(request, problem_id):
    problem = get_object_or_404(Problem, pk=problem_id)
    if request.method == 'POST':
        form = ProblemCloseForm(problem, request.POST)
        if form.is_valid():
            problem.close(form.cleaned_data['ended_at'])
            problem.save()
            
            comment = Comment()
            comment.author = request.user
            comment.problem = problem
            comment.link = form.cleaned_data['link']
            comment.comment = "%s\nClosed the problem with end date: %s" % (
                form.cleaned_data['comment'],
                form.cleaned_data['ended_at'],
            )
            
            comment.save()

            uri = reverse('problem_detail', kwargs={'problem_id':problem_id})
            uri += '?' + http.urlencode({'closed': comment.id})
            return redirect(uri)
        else:
            # Not a valid form submission
            return render_to_response('problemdb/problem_close.html', {
                'form': form,
                'problem': problem,
            }, context_instance=RequestContext(request))
    else:
        form = ProblemCloseForm(problem)
        return render_to_response('problemdb/problem_close.html',{
            'form': form,
            'problem': problem
        }, context_instance=RequestContext(request))

def comments(request, problem_id):
    if request.method == 'POST':
        form = CommentForm(request.POST)
        problem = get_object_or_404(Problem, pk=problem_id)
        if form.is_valid():
            comment = form.save(commit=False)
            comment.problem_id = problem_id
            comment.author = request.user
            comment.save()

            uri = reverse('problem_detail', kwargs={'problem_id':problem_id})
            uri += '?' + http.urlencode({'new-comment': comment.id})
            return redirect(uri)
        else:
            # Not a valid form submission
            return render_to_response('problemdb/problem_detail.html', {
                'problem': problem,
                'comments': problem.comments.all(),
                'comment_form': form,
            }, context_instance=RequestContext(request))

    else:
        return http.HttpResponseServerError('Method is not supported ')

def problems_print(request):
    
    systems = System.objects.filter(visible__exact=True)

    for s in systems:
        s.active_problems = s.problems.filter(ended_at__isnull=True).order_by('-added_at')

    return render_to_response('problemdb/report.html',{
            'systems': systems
        }, context_instance=RequestContext(request))


def _api_response(request, result):
  """
  Creates the response object for the api_* views serializing the result as a
  JSON object.

  If the argument 'callback' is included in the request, the response uses the
  JSONP pattern and the argument value is used as the callback function.
  """
  callback = request.GET.get("callback")

  res = http.HttpResponse(mimetype='application/json')

  if callback:
    res.write("%s(" % callback)
    #res.write(json)
    simplejson.dump(result, res)
    res.write(")")
  else:
    #res.write(json)
    simplejson.dump(result, res)

  return res 

def _api_problem_as_json(problem):
    """
    Creates a dictionary from a Problem object so that it can be serialized with JSON
    """
    result = {}

    attrs = ('id', 'title', 'severity', 'system')
    for a in attrs:
        result[a] = unicode(getattr(problem, a))

    attrs = ('started_at', 'ended_at', 'added_at')
    for a in attrs:
        if getattr(problem, a):
            result[a] = getattr(problem, a).strftime("%Y-%m-%dT%H:%M:%S%z")

    return result

def api_search(request):
    filters, problems = _search(request)
    return _api_response(request, map(_api_problem_as_json, problems))

def api_systems(request):

    #format = request.GET.get('format', 'csv')
    #
    #response = http.HttpResponse()
    #if format == 'csv':
    #    response['content-type'] = 'text/plain'
    #    
    #    writer = csv.writer(response)
    #    print dir(writer)
    #    #writer.writerow(('name',))
    #    for system in System.objects.all():
    #        writer.writerow((system.name,))
    #    return response

    #return http.HttpResponse('Error!')
    

    ss = System.objects.all()
    return http.HttpResponse( ' '.join(map(getattr,ss,['name']*len(ss))) )

def api_activity(request):

    format = request.GET.get('format', 'csv')
    
    yesterday = datetime.datetime.now()-datetime.timedelta(days=1)
    started_at = request.GET.get('started_at', yesterday)
    ended_at   = request.GET.get('ended_at', datetime.datetime.now() )

    last_comments   = Comment.objects.filter(added_at__gte=started_at).filter(added_at__lte=ended_at).all()
    
    active_problems = {}
    
    for lc in last_comments:
        active_problems[lc.problem.id] = lc.problem

    for ap in Problem.objects.filter(ended_at__isnull=True, system__visible__exact=True).order_by('-started_at').select_related():
        active_problems[ap.id] = ap
    
    return _api_response(request, map(_api_problem_as_json, active_problems.values()))

def api_create_problem(request):
    """
    Creates a Problem record from the data provided in the POST request.
    
    An 'apikey' is used to authenticate the user and the user name has to be
    provided in the request.
    
    Reponse types:
    
    - The response has the status code 201 if the problem record is created
    successfully. 
    - Forbidden (403) if the apikey is invalid
    - Not found (404) if the system name is not available
    
    """
    if request.method == 'POST':

        if request.META['CONTENT_TYPE']==None: request.META['CONTENT_TYPE']=''

        post_data = request.POST.copy()
        extra_data = {}
        apikey = post_data.get('apikey')
        if ApiKey.objects.filter(value__exact=apikey).count() == 0:
            msg = "Invalid API key: '%s'\n" % apikey
            return http.HttpResponseForbidden(msg, mimetype="text/plain")

        if not post_data.get('severity'):
            severity_id = Severity.objects.order_by('-ordering')[0].id
        else:
            ss = Severity.objects.filter(name=post_data.get('severity')).all()
            if len(ss)!=1:
                msg = "Bad severity name='%s'\n" % post_data.get('severity')
                return http.HttpResponseNotFound(msg, mimetype="text/plain")
            severity_id = ss[0].id
        extra_data['severity'] = severity_id

        if not post_data.get('started_at'):
            now = datetime.datetime.now()
            fmt = "%s %s" % (settings.FORM_DATE_FORMAT, settings.FORM_TIME_FORMAT)
            extra_data['started_at'] = datetime.datetime.strftime(now, fmt)


        if post_data.get('system_name') and not post_data.get('system'):

            try:
                system = System.objects.get(name=post_data.get('system_name'))
                extra_data['system'] = system.id
            except System.objects.model.DoesNotExist:
                msg = "System not found. name='%s'\n" % post_data.get('system_name')
                return http.HttpResponseNotFound(msg, mimetype="text/plain")

        post_data.update(extra_data)

        form = ApiProblemCreateForm(post_data)
        if form.is_valid():
            problem = form.save(commit=False)
            #problem.author = request.user
            problem.author_name = form.cleaned_data['author_name']
            problem.save()
            form.save_m2m()

            comment = Comment()
            comment.problem = problem
#            comment.author = request.user
            comment.author_name = form.cleaned_data['author_name']
            comment.comment = form.cleaned_data["initial_comment"]
            comment.link = form.cleaned_data.get("link")

            comment.save()

            uri = reverse('problem_detail', kwargs={'problem_id':problem.id})
            response = http.HttpResponse(status=201)
            response['Location'] = uri

            return response
            #return redirect(uri)
        else:
            import pprint
            return http.HttpResponseBadRequest(pprint.pformat(form.errors))
    else:
        return http.HttpResponseServerError('Method is not supported', status=405)
