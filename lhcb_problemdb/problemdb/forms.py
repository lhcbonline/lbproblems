# -*- coding: utf-8 -*-

import datetime

from django import forms
from django.forms import ModelForm

from django.conf import settings

from models import Comment, Problem, Severity

class ProblemForm(ModelForm):
    started_at = forms.SplitDateTimeField(label="Started at", required=True,
        initial=datetime.datetime.now,
        widget = forms.widgets.SplitDateTimeWidget(
            date_format=settings.FORM_DATE_FORMAT,
            time_format=settings.FORM_TIME_FORMAT,
        ))

    ended_at = forms.SplitDateTimeField(label="Ended at", required=False,
        widget = forms.widgets.SplitDateTimeWidget(
            date_format=settings.FORM_DATE_FORMAT,
            time_format=settings.FORM_TIME_FORMAT,
        ))

    def clean(self):

        data = self.cleaned_data

        ended_at = data.get("ended_at")
        severity = data.get("severity")

        if ended_at and not severity.assessed:
            valid_severities = Severity.objects.filter(assessed__exact = True).all()
            
            raise forms.ValidationError("The end date can be set only once the severity has been assessed. Change the severity to '%s' or do not enter an end date. " % valid_severities[0].name)
        
        return data

    def clean_ended_at(self):        
        started_at = self.cleaned_data.get("started_at")
        ended_at = self.cleaned_data.get("ended_at")
        if ended_at and ended_at < started_at:
            raise forms.ValidationError("The end date must be after the start date")

        return ended_at

    class Meta:
        model = Problem
        fields = ('title', 'system', 'severity', 'started_at', 'ended_at')


class ProblemCreateForm(ProblemForm):

    initial_comment = forms.CharField(required=True, widget=forms.Textarea)
    link = forms.URLField(required=False, label="Logbook link")
    class Meta(ProblemForm.Meta):
        fields = ('title', 'system', 'severity', 'started_at', 'ended_at', 'initial_comment')

class ProblemCloseForm(forms.Form):
    comment = forms.CharField(required=True, widget=forms.Textarea)
    ended_at = forms.SplitDateTimeField(label="Ended at", required=True,
        initial=datetime.datetime.now,
        widget = forms.widgets.SplitDateTimeWidget(
            date_format=settings.FORM_DATE_FORMAT,
            time_format=settings.FORM_TIME_FORMAT,
        ))
    link = forms.URLField(required=False, label="Logbook link")


    def __init__( self, problem, *args, **kwargs ): 
        """
        Using additional argument for validating the end date with the
        problem's start date
        """
        super(ProblemCloseForm, self ).__init__( *args, **kwargs) 
        self.problem = problem

    def clean_ended_at(self):
        ended_at = self.cleaned_data.get("ended_at")
        if ended_at < self.problem.started_at:
            raise forms.ValidationError("The end date must be after the start date") 

        return ended_at

class CommentForm(ModelForm):

    class Meta:
        model = Comment
        fields = ('comment','link')

class ApiProblemCreateForm(ProblemCreateForm):
    started_at = forms.DateTimeField(label="Started at", required=True,
        initial=datetime.datetime.now)
    system_name = forms.CharField(required=False, widget=forms.Textarea)
    author_name = forms.CharField(required=True, widget=forms.Textarea)

